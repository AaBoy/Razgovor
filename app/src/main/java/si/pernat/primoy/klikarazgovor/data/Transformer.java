package si.pernat.primoy.klikarazgovor.data;

import android.graphics.Bitmap;

/**
 * Created by Primoz on 19.12.2015.
 */
public interface Transformer {
    Bitmap  transform(int inputParam);
}
