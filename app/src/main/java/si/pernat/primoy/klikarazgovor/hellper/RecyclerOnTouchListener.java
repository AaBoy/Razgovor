package si.pernat.primoy.klikarazgovor.hellper;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

public class RecyclerOnTouchListener implements RecyclerView.OnItemTouchListener {

    private GestureDetector gestureDetector;
    private ClickListener mClickListener;

    public RecyclerOnTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {

        mClickListener = clickListener;
        gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                super.onLongPress(e);
                View childe = recyclerView.findChildViewUnder(e.getX(), e.getY());

                if (childe != null && clickListener != null) {
                    clickListener.onLongClick(childe, recyclerView.getChildAdapterPosition(childe));
                }
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
        View childe = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());
        if (childe != null && mClickListener != null && gestureDetector.onTouchEvent(motionEvent)) {
            mClickListener.onClick(childe, recyclerView.getChildAdapterPosition(childe));
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean b) {

    }
}