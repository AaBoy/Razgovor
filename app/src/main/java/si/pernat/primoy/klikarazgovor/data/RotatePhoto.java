package si.pernat.primoy.klikarazgovor.data;

import android.graphics.Bitmap;
import android.graphics.Matrix;

/**
 * Created by Primoz on 21.12.2015.
 */
public class RotatePhoto extends Photo implements Transformer {

    public RotatePhoto(){
        super();
    }
    public RotatePhoto(int id, int albumId, String title, String url, String thumbnailUrl) {
        super(id, albumId, title, url, thumbnailUrl);
    }

    @Override
    public Bitmap transform(int inputParam) {
        Matrix matrix = new Matrix();
        matrix.postRotate(inputParam);
        Bitmap tmp=getBitmap();
        if(tmp!=null){
            tmp= Bitmap.createBitmap(getBitmap(),0,0,tmp.getWidth(),tmp.getHeight(),matrix,true);
            setBitmap(tmp);
            return tmp;
        }
        return null;
    }
}
