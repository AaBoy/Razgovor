package si.pernat.primoy.klikarazgovor.asyncTask;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by Primoz on 20.12.2015.
 */
public class DownloadImage extends AsyncTask<String, String, Bitmap> {

    public AsyncResponse response = null;

    public DownloadImage(AsyncResponse response) {
        this.response=response;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        URL url = null;
        try {
            url = new URL(params[0]);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setInstanceFollowRedirects(false);
            url = new URL(conn.getHeaderField("Location"));
            conn = (HttpURLConnection) url.openConnection();
            conn.connect();
            return BitmapFactory.decodeStream(conn.getInputStream());

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        response.processFinish(bitmap);
    }

    public interface AsyncResponse {
        void processFinish(Bitmap bitmap);
    }
}
