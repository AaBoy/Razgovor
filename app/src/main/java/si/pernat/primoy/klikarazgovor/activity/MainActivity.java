package si.pernat.primoy.klikarazgovor.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;


import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import si.pernat.primoy.klikarazgovor.data.RotatePhoto;
import si.pernat.primoy.klikarazgovor.R;
import si.pernat.primoy.klikarazgovor.adapter.PhotoRecyclerViewAdapter;
import si.pernat.primoy.klikarazgovor.hellper.ClickListener;
import si.pernat.primoy.klikarazgovor.data.Photo;
import si.pernat.primoy.klikarazgovor.hellper.RecyclerOnTouchListener;
import si.pernat.primoy.klikarazgovor.volley.VolleySingleton;

public class MainActivity extends AppCompatActivity {

    private static final String URL = "http://www.json-generator.com/api/json/get/cffPPZnGnC?indent=2";
    private static final String TAG = "ERROR_RESPONSE";

    PhotoRecyclerViewAdapter mAdapter;
    RecyclerView mRecyclerView;
    ArrayList<Photo> mPhotos;
    RequestQueue mRequestQueue;
    Toolbar toolbar;
    ProgressDialog progressDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Primož Pernat");
        setSupportActionBar(toolbar);

        mRequestQueue = VolleySingleton.getInstance(this).getRequestQueue();
        progressDialog = new ProgressDialog(MainActivity.this);
        getData();
    }

    private void startActivity(String url) {
        Intent intent = new Intent(this, WebViewAcitivity.class);
        intent.putExtra(WebViewAcitivity.URL_PHOTO, url);
        startActivity(intent);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_refresh) {
            getData();
        }
        return true;
    }


    public void getData() {

        progressDialog.setMessage(getString(R.string.retrieving_data));
        progressDialog.show();
        StringRequest request = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Gson gson = new Gson();
                        mPhotos = gson.fromJson(response, new TypeToken<ArrayList<RotatePhoto>>() {
                        }.getType());
                        setUpRecyclerView();
                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                        Snackbar.make(findViewById(android.R.id.content), R.string.volley_error_message, Snackbar.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }
                });
        request.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setShouldCache(false);
        mRequestQueue.add(request);
    }

    private void setUpRecyclerView() {
        if (mPhotos != null && mRecyclerView != null) {
            mAdapter = new PhotoRecyclerViewAdapter(getBaseContext(), mPhotos);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
            mRecyclerView.addOnItemTouchListener(new RecyclerOnTouchListener(this, mRecyclerView, new ClickListener() {
                @Override
                public void onClick(View view, int position) {
//                    Snackbar.make(findViewById(android.R.id.content), "Pritisno sem nekaj" + position, Snackbar.LENGTH_LONG).show();
                    startActivity(mPhotos.get(position).getUrl());
                }

                @Override
                public void onLongClick(View view, int position) {

                }
            }));
            mRecyclerView.setAdapter(mAdapter);
        }
    }
}
