package si.pernat.primoy.klikarazgovor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;

import java.util.ArrayList;
import java.util.Random;

import si.pernat.primoy.klikarazgovor.R;
import si.pernat.primoy.klikarazgovor.data.Photo;
import si.pernat.primoy.klikarazgovor.volley.VolleySingleton;

/**
 * Created by Primoz on 19.12.2015.
 */
public class PhotoRecyclerViewAdapter extends RecyclerView.Adapter<PhotoRecyclerViewAdapter.PhotoItem> {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    ArrayList<Photo> mPhotos;
    ImageLoader imageLoader;
    VolleySingleton volleySingleton;


    public PhotoRecyclerViewAdapter(Context context, ArrayList<Photo> photos) {
        mLayoutInflater = LayoutInflater.from(context);
        mPhotos = photos;
        volleySingleton=VolleySingleton.getInstance(mContext);
        imageLoader = volleySingleton.getImageLoader();
        this.mContext=context;
    }

    @Override
    public PhotoItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_photo, parent, false);
        return new PhotoItem(view);
    }

    @Override
    public void onBindViewHolder(final PhotoItem holder, final int position) {

        if(holder.imageView.getDrawable()!=null){
            holder.imageView.setImageDrawable(null);
        }
        holder.textView.setText(mPhotos.get(position).getTitle());
        String urlThumbnail = mPhotos.get(position).getThumbnailUrl();

        if (urlThumbnail != null) {
            imageLoader.get(urlThumbnail, new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("ErrorImageLoader",error.toString());
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    if(mPhotos.get(position).getBitmap()==null){
                        mPhotos.get(position).setBitmap(response.getBitmap());
                    }
                    holder.imageView.setImageBitmap(mPhotos.get(position).transform(new Random(System.currentTimeMillis()).nextInt(360)));

                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return mPhotos.size();
    }

    class PhotoItem extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;

        public PhotoItem(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.imageView);
            textView = (TextView) view.findViewById(R.id.textview);
        }
    }

}
