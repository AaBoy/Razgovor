package si.pernat.primoy.klikarazgovor.hellper;

import android.view.View;

/**
 * Created by Primoz on 16.9.2015.
 */
public interface ClickListener {
    public void onClick(View view, int position);

    public void onLongClick(View view, int position);
}
