package si.pernat.primoy.klikarazgovor.data;

import android.graphics.Bitmap;
import android.util.LruCache;

/**
 * Created by Primoz on 19.12.2015.
 */
public abstract class Photo implements Transformer {
    private int id;
    private int albumId;
    private String title;
    private String url;
    private String thumbnailUrl;
    public static LruCache<String, Bitmap> lureCache;

    public Photo() {
        if (lureCache == null) {
            lureCache = new LruCache<>((int) (Runtime.getRuntime().maxMemory() / 1024 / 8));
        }
    }

    public Photo(int id, int albumId, String title, String url, String thumbnailUrl) {
        super();
        this.id = id;
        this.albumId = albumId;
        this.title = title;
        this.url = url;
        this.thumbnailUrl = thumbnailUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAlbumId() {
        return albumId;
    }

    public void setAlbumId(int albumId) {
        this.albumId = albumId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public Bitmap getBitmap() {
        return lureCache.get(thumbnailUrl);
    }

    public void setBitmap(Bitmap bitmap) {
        if(lureCache!=null && bitmap!=null && lureCache.get(thumbnailUrl)==null){
            lureCache.put(thumbnailUrl, bitmap);
        }
    }
}
