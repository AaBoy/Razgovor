package si.pernat.primoy.klikarazgovor.activity;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import si.pernat.primoy.klikarazgovor.R;
import si.pernat.primoy.klikarazgovor.asyncTask.DownloadImage;

public class WebViewAcitivity extends AppCompatActivity implements DownloadImage.AsyncResponse {
    final public static String URL_PHOTO = "url_photo";
    private WebView mWebView;
    final private static  int REQUEST_WRITE_READ=133;
    String url;
    Bitmap mBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view_acitivity);
        mWebView = (WebView) findViewById(R.id.webview);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            url = extras.getString(URL_PHOTO);
            DownloadImage downloadImageAsyncTask = new DownloadImage(this);
            downloadImageAsyncTask.execute(url);
            Log.d("URL",url);
            mWebView.setWebViewClient(new Callback());
            mWebView.loadUrl(url);
        }

    }

    @Override
    public void processFinish(Bitmap bitmap) {
        mBitmap = bitmap;
    }


    class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.webview_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.menu_item_share) {
            if (url != null) {
                if (Build.VERSION_CODES.M <= Build.VERSION.SDK_INT) {
                    boolean hasPermission = (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
                    if (!hasPermission) {
                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                REQUEST_WRITE_READ);
                    }else{
                        sendImage();
                    }
                }
            } else {
                Snackbar.make(findViewById(android.R.id.content), R.string.no_image, Snackbar.LENGTH_LONG).show();
            }
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case REQUEST_WRITE_READ:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    sendImage();
                } else
                {
                    Snackbar.make(findViewById(android.R.id.content), "You have don't allow read and write functionality", Snackbar.LENGTH_LONG).show();
                }
        }
    }

    private void sendImage() {

        final File file=new File(saveToInternalStorage(mBitmap));
        Uri uri = Uri.fromFile(file);
        Log.d("URI", uri.toString());
        Intent shareIntent = new Intent();
        Intent chooser = null;
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("image/png");
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        shareIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        chooser = Intent.createChooser(shareIntent, "Send Image");
        startActivityForResult(chooser, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    private String saveToInternalStorage(Bitmap bitmapImage) {
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_images");
        myDir.mkdirs();

        String fname = "Test2.png";
        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("ERROR",e.toString());
        }
        return file.toString();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        File file = new File(extStorageDirectory, "Test2" + ".png");
        if (file.exists()) {
            file.delete();
        }
    }
}
